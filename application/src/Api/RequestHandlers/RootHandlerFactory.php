<?php

declare(strict_types=1);

namespace Api\RequestHandlers;

use Api;
use Web;
use Authentication;

class RootHandlerFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        $dbService = $serviceManager->getService(Api\Services\DataBaseService::class);
        $jwtService = $serviceManager->getService(Api\Services\JwtService::class);

        return new RootHandler(
            new Authentication\Service(
                new Authentication\RelationalDataAccess($dbService->getConnection()),
                new Authentication\Credentials\RelationalDataAccess($dbService->getConnection()),
                $jwtService->getGenerator()
            )
        );
    }
}
