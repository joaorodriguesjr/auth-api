<?php

namespace Api\RequestHandlers;

use Web;
use Authentication;

class RootHandler implements Web\RequestHandler
{
    private $authenticationService;

    public function __construct(Authentication\Service $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function handle(Web\Request $request, Web\Response $response)
    {
        if (! $response->isSuccessful()) {
            return;
        }

        switch ($request->getMethod()) {
            case 'GET':
            case 'OPTIONS':
                $response->setStatusCode($response::HTTP_NO_CONTENT);
                break;
            case 'POST':
                $this->post($request, $response);
                break;
            case 'PUT':
                $this->put($request, $response);
                break;
            case 'DELETE':
                $this->delete($request, $response);
                break;
        }
    }

    private function post(Web\Request $request, Web\Response $response)
    {
        try {
            $this->authenticationService->authenticate($request->attributes->get('credentials'));
            $auth = $this->authenticationService->getAuthenticationData();

            $responseData = [
                'token'   => $auth['token'  ],
                'refresh' => $auth['key'    ],
                'expires' => $auth['expires'],
            ];

            $responseCode = $response::HTTP_OK;
        } catch (Authentication\Exception $exception) {
            $responseData = ['error' => 'Authentication not performed'];
            $responseCode = $response::HTTP_BAD_REQUEST;
        }

        $response->setStatusCode($responseCode)->setContent(serialize($responseData));
    }

    private function put(Web\Request $request, Web\Response $response)
    {
        try {
            $this->authenticationService->refreshAuthentication($request->attributes->get('Refresh'));
            $auth = $this->authenticationService->getAuthenticationData();

            $responseData = [
                'token'   => $auth['token'  ],
                'refresh' => $auth['key'    ],
                'expires' => $auth['expires'],
            ];

            $responseCode = $response::HTTP_OK;
        } catch (Authentication\Exception $exception) {
            $responseData = ['error' => 'Refresh not performed'];
            $responseCode = $response::HTTP_BAD_REQUEST;
        }

        $response->setStatusCode($responseCode)->setContent(serialize($responseData));
    }

    private function delete(Web\Request $request, Web\Response $response)
    {
        $key = $request->attributes->get('Refresh');

        try {
            $this->authenticationService->invalidateAuthentication($key);

            $responseData = ['success' => 'Authentication invalidated'];
            $responseCode = $response::HTTP_OK;
        } catch (Authentication\Exception $exception) {
            $responseData = ['error' => 'Invalidation not performed'];
            $responseCode = $response::HTTP_BAD_REQUEST;
        }

        $response->setStatusCode($responseCode)->setContent(serialize($responseData));
    }
}
