<?php

namespace Api\Services;

use Web;
use Authentication;
use Lcobucci\JWT;

class JwtServiceFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        $tokenGenerator = new Authentication\Token\JWTGenerator(
            new JWT\Parser(),
            new JWT\Builder(),
            new JWT\Signer\Rsa\Sha256(),
            new JWT\Signer\Key('file://' . __DIR__ . '/../../../data/jwt/private.key'),
            new JWT\Signer\Key('file://' . __DIR__ . '/../../../data/jwt/public.key')
        );

        return new JwtService($tokenGenerator);
    }
}
