<?php

namespace Api\Services;

use PDO;
use Web;

class DataBaseService implements Web\Service
{
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }
}
