<?php

namespace Api\Services;

use PDO;
use Web;

class DataBaseServiceFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        $dsn = 'mysql:host=database;charset=utf8;dbname=' . getenv('DATABASE_NAME');
        $connection = new PDO($dsn, getenv('DATABASE_USER'), getenv('DATABASE_PASS'));

        $connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $connection->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        return new DataBaseService($connection);
    }
}
