<?php

namespace Api\Services;

use Web;
use Authentication;

class JwtService implements Web\Service
{
    private $generator;

    public function __construct(Authentication\Token\Generator $generator)
    {
        $this->generator = $generator;
    }

    public function getGenerator(): Authentication\Token\Generator
    {
        return $this->generator;
    }
}
