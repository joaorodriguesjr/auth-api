<?php

namespace Api\EventHandlers;

use Web;

class CookieSender implements Web\EventHandler
{
    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\PostDispatch) {
            return;
        }

        if (! $event->response->isSuccessful()) {
            return;
        }

        $data = unserialize($event->response->getContent());

        if ($event->request->getPathInfo() === '/' && $event->request->getMethod() === 'DELETE') {
            $this->unsetCookies($event->response);
        }

        if ($event->request->getMethod() === 'POST' || $event->request->getMethod() === 'PUT') {
            $this->setCookies($event->response, $data);
        }
    }

    private function setCookies(Web\Response $response, array $data)
    {
        $cookies = [];
        $expires = date(DATE_COOKIE, strtotime($data['expires']));

        $cookies[] = 'Refresh=' . $data['refresh']
            . ';Expires=' . $expires
            . ';Secure;HttpOnly;SameSite=None';

        $response->headers->set('Set-Cookie', $cookies);
    }

    private function unsetCookies(Web\Response $response)
    {
        $cookies = [];
        $expires = date(DATE_COOKIE, strtotime('- 10 days'));

        $cookies[] = 'Refresh=;Expires=' . $expires
            . ';Secure;HttpOnly;SameSite=None';

        $response->headers->set('Set-Cookie', $cookies);
    }
}
