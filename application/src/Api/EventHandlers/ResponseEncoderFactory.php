<?php

namespace Api\EventHandlers;

use Web;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ResponseEncoderFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        return new ResponseEncoder(new Serializer([], $encoders));
    }
}
