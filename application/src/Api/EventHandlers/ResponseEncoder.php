<?php

namespace Api\EventHandlers;

use Web;
use Symfony\Component\Serializer\Serializer;

class ResponseEncoder implements Web\EventHandler
{
    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\PostDispatch) {
            return;
        }

        $contentType = 'text/plain';
        $event->response->headers->set('Content-Type', $contentType);

        $data = @unserialize($event->response->getContent());

        if (empty($data) && ! is_array($data)) {
            return;
        }

        $accepted = $event->request->headers->get('Accept');
        $formats = ['xml' => 'application/xml', 'json' => 'application/json'];
        $encoded = '';

        foreach ($formats as $format => $type) {
            if (! preg_match("#$type#", $accepted)) {
                continue;
            }

            $contentType = $type;
            $encoded = $this->serializer->encode($data, $format);
        }

        $event->response->headers->set('Content-Type', $contentType);

        if (! empty($encoded)) {
            $event->response->setContent($encoded);
        }
    }
}
