<?php

namespace Api\EventHandlers;

use Web;

class CorsPolicy implements Web\EventHandler
{
    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\PreDispatch) {
            return;
        }

        $event->response->headers->add([
            'Access-Control-Allow-Origin'  => 'http://localhost:4200',
            'Access-Control-Allow-Methods' => 'OPTIONS, GET, POST, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Authorization',
        ]);
    }
}
