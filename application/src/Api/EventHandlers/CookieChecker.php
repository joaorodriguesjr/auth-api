<?php

namespace Api\EventHandlers;

use Web;

class CookieChecker implements Web\EventHandler
{
    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\PreDispatch) {
            return;
        }

        if ($event->request->getPathInfo() !== '/') {
            return;
        }

        switch ($event->request->getMethod()) {
            case 'OPTIONS':
            case 'GET':
            case 'POST':
                return;
        }

        $cookies = $event->request->cookies->all();

        if (array_key_exists('Refresh', $cookies)) {
            $event->request->attributes->add($cookies);
            return;
        }

        $responseData = ['error' => 'Missing required cookie'];
        $responseCode = Web\Response::HTTP_BAD_REQUEST;

        $event->response->setStatusCode($responseCode)->setContent(serialize($responseData));
    }
}
