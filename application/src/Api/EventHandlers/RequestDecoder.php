<?php

namespace Api\EventHandlers;

use Web;
use Symfony\Component\Serializer\Serializer;

class RequestDecoder implements Web\EventHandler
{
    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\PreDispatch) {
            return;
        }

        $contentType = $event->request->headers->get('Content-Type');

        if (empty($contentType)) {
            return;
        }

        $formats = ['application/xml' => 'xml', 'application/json' => 'json'];

        if (! array_key_exists($contentType, $formats)) {
            return;
        }

        $data = $this->serializer->decode($event->request->getContent(), $formats[$contentType]);
        $event->request->attributes->add($data);
    }
}
