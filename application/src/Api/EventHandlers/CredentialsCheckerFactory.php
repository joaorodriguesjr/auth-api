<?php

namespace Api\EventHandlers;

use Web;

class CredentialsCheckerFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        return new CredentialsChecker();
    }
}
