<?php

namespace Api\EventHandlers;

use Web;

class CredentialsChecker implements Web\EventHandler
{
    public function handle(Web\Event $event)
    {
        if (!$event instanceof Web\Event\PreDispatch) {
            return;
        }

        if ($event->request->getPathInfo() !== '/' || $event->request->getMethod() !== 'POST') {
            return;
        }

        $credentials = [
            'identity' => $event->request->server->get('PHP_AUTH_USER'),
            'password' => $event->request->server->get('PHP_AUTH_PW'),
        ];

        if (empty($credentials['identity']) || empty($credentials['password'])) {
            $event->response->setStatusCode(Web\Response::HTTP_BAD_REQUEST)
                ->setContent('Missing user credentials.');
            return;
        }

        $event->request->attributes->set('credentials', $credentials);
    }
}
