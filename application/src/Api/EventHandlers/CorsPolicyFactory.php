<?php

namespace Api\EventHandlers;

use Web;

class CorsPolicyFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        return new CorsPolicy();
    }
}
