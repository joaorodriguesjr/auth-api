<?php

namespace Api\EventHandlers;

use Web;

class CookieCheckerFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        return new CookieChecker();
    }
}
