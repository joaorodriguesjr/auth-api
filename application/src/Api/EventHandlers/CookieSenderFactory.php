<?php

namespace Api\EventHandlers;

use Web;

class CookieSenderFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        return new CookieSender();
    }
}
