<?php

namespace Authentication\Credentials;

use PDO;

class RelationalDataAccess implements DataAccess
{
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function fetchByIdentity(string $identity): array
    {
        $sql = '
            SELECT `identity`, `password`, LOWER(HEX(`owner`)) AS `owner`
            FROM `credentials`
            WHERE `identity`=:identity;
        ';

        $statement = $this->connection->prepare($sql);
        $statement->execute(['identity' => $identity]);
        $data = $statement->fetch();

        return empty($data) ? [] : $data;
    }
}
