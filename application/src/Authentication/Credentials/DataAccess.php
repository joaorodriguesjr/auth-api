<?php

namespace Authentication\Credentials;

interface DataAccess
{
    public function fetchByIdentity(string $username) : array;
}
