<?php

namespace Authentication;

class Service
{
    private $authenticationDataAccess;

    private $credentialsDataAccess;

    private $tokenGenerator;

    private $data;

    public function __construct(
        DataAccess $authenticationDataAccess,
        Credentials\DataAccess $credentialsDataAccess,
        Token\Generator $tokenGenerator
    ) {
        $this->credentialsDataAccess = $credentialsDataAccess;
        $this->authenticationDataAccess = $authenticationDataAccess;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function getAuthenticationData() : array
    {
        return $this->data;
    }

    public function authenticate(array $credentials)
    {
        $data = $this->credentialsDataAccess->fetchByIdentity($credentials['identity']);

        if (empty($data)) {
            throw new Exception('Credentials not found for: ' . $credentials['identity']);
        }

        if (! password_verify($credentials['password'], $data['password'])) {
            throw new Exception('Invalid password for: ' . $credentials['identity']);
        }

        $this->generateAuthentication($data['owner']);
    }

    public function refreshAuthentication(string $key)
    {
        $data = $this->authenticationDataAccess->fetch($key);

        if (empty($data)) {
            throw new Exception('Invalid authentication. KEY: ' . $key);
        }

        $this->authenticationDataAccess->invalidate($key);

        if (strtotime($data['expires']) < time()) {
            throw new Exception('Expired authentication. KEY: ' . $key);
        }

        $this->generateAuthentication($data['owner']);
    }

    public function invalidateAuthentication(string $key)
    {
        $data = $this->authenticationDataAccess->fetch($key);

        if (empty($data)) {
            throw new Exception('Invalid authentication. KEY: ' . $key);
        }

        $this->authenticationDataAccess->invalidate($key);
    }

    private function generateAuthentication(string $user)
    {
        $data = [
            'key'     => $this->generateKey(),
            'token'   => $this->tokenGenerator->generate($user),
            'expires' => date('Y-m-d H:i:s', strtotime('now + 1 day')),
            'owner'   => $user,
        ];

        $this->authenticationDataAccess->store($data);
        $this->data = $data;
    }

    private function generateKey() : string
    {
        $data = openssl_random_pseudo_bytes(16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        $uuid = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
        return str_replace('-', '', $uuid);
    }
}
