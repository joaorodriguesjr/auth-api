<?php

namespace Authentication;

interface DataAccess
{
    public function store(array $data);

    public function fetch(string $key) : array;

    public function invalidate(string $key);
}
