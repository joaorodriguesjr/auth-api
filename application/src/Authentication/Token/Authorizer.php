<?php

namespace Authentication\Token;

interface Authorizer
{
    /**
     * @param string $accessToken
     *
     * @return boolean
     *
     * @throws \Authentication\Token\Exception
     */
    public function authorize(string $accessToken): bool;
}
