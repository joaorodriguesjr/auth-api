<?php

namespace Authentication\Token;

use Authentication;
use Lcobucci\JWT;

class JWTAuthorizer implements Authorizer
{
    private $parser;

    private $signer;

    private $key;

    private $validationData;

    public function __construct(JWT\Parser $parser, JWT\Signer $signer, JWT\Signer\Key $key)
    {
        $this->parser = $parser;
        $this->signer = $signer;
        $this->key    = $key;
    }

    /**
     * @param string $accessToken
     *
     * @return boolean
     *
     * @throws \Authentication\Token\Exception
     */
    public function authorize(string $accessToken): bool
    {
        try {
            $token = $this->parser->parse($accessToken);
        } catch (\Exception $exception) {
            throw new Authentication\Token\Exception($exception->getMessage());
        }

        $valid = $token->validate($this->getValidationData());
        $verified = $token->verify($this->signer, $this->key);

        return $valid && $verified;
    }

    public function setValidationData(JWT\ValidationData $validationData)
    {
        $this->validationData = $validationData;
    }

    public function getValidationData() : JWT\ValidationData
    {
        if (! isset($this->validationData)) {
            $this->validationData = new JWT\ValidationData();
        }

        return $this->validationData;
    }
}
