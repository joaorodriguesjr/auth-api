<?php

namespace Authentication\Token;

use Lcobucci\JWT;

class JWTGenerator implements Generator
{
    private $parser;

    private $builder;

    private $signer;

    private $privateKey;

    private $publicKey;

    public function __construct(
        JWT\Parser $parser,
        JWT\Builder $builder,
        JWT\Signer $signer,
        JWT\Signer\Key $privateKey,
        JWT\Signer\Key $publicKey
    ) {
        $this->parser  = $parser;
        $this->builder = $builder;
        $this->signer  = $signer;
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
    }

    public function generate(string $user, int $time = null) : string
    {
        if ($time === null) {
            $time = time();
        }

        return (string) $this->builder
            ->issuedAt($time)
            ->expiresAt($time + 300)
            ->relatedTo($user)
            ->getToken($this->signer, $this->privateKey);
    }

    public function parse(string $token)
    {
        try {
            $jwt = $this->parser->parse($token);
        } catch (\Exception $exception) {
            throw new Exception($exception->getMessage());
        }

        return $jwt;
    }

    public function regenerate(string $token) : string
    {
        $jwt = $this->parse($token);

        if (! $jwt->verify($this->signer, $this->publicKey)) {
            throw new Exception('Invalid token signature');
        }

        $user = $jwt->getClaim('sub');
        return $this->generate($user);
    }

    public function extractSubject(string $token): string
    {
        $jwt = $this->parse($token);
        return $jwt->getClaim('sub');
    }
}
