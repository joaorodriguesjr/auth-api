<?php

namespace Authentication\Token;

interface Generator
{
    public function generate(string $user) : string;

    public function regenerate(string $token) : string;

    public function extractSubject(string $token) : string;
}
