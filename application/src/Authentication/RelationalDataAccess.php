<?php

namespace Authentication;

use PDO;

class RelationalDataAccess implements DataAccess
{
    private $connection;

    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function store(array $data)
    {
        $sql = '
            INSERT INTO `authentications` (`id`, `token`, `expires`, `owner`) 
            VALUES (UNHEX(:key), :token, :expires, UNHEX(:owner));
        ';

        $statement = $this->connection->prepare($sql);
        $statement->execute($data);
    }

    public function fetch(string $key): array
    {
        $sql = '
            SELECT LOWER(HEX(`id`)) AS `key`, `token`, `expires`, LOWER(HEX(`owner`)) AS `owner`
            FROM `authentications`
            WHERE `id`=UNHEX(:key) AND `invalidated` IS NULL;
        ';

        $statement = $this->connection->prepare($sql);
        $statement->execute(['key' => $key]);
        $data = $statement->fetch();

        return $data ? $data : [];
    }

    public function invalidate(string $key)
    {
        $sql = '
            UPDATE `authentications` SET `invalidated`=NOW()
            WHERE `id`=UNHEX(:key);
        ';

        $statement = $this->connection->prepare($sql);
        $statement->execute(['key' => $key]);
    }
}
