<?php

namespace Web;

interface RequestHandler extends Service
{
    public function handle(Request $request, Response $response);
}
