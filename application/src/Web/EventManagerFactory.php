<?php

namespace Web;

class EventManagerFactory implements ServiceFactory
{
    public function create(ServiceManager $serviceManager): Service
    {
        $configuration = $serviceManager->getConfig('events');
        $subscriptions = [];

        foreach ($configuration as $event => $handlers) {
            $subscriptions[$event] = [];

            foreach ($handlers as $handler) {
                $subscriptions[$event][] = $serviceManager->getService($handler);
            }
        }

        return new EventManager($subscriptions);
    }
}
