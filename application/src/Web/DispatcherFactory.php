<?php

namespace Web;

class DispatcherFactory implements ServiceFactory
{
    public function create(ServiceManager $serviceManager): Service
    {
        return new Dispatcher($serviceManager->getService(EventManager::class));
    }
}
