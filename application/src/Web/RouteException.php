<?php

namespace Web;

class RouteException extends Exception
{
    const NOT_FOUND   = 404;

    const NOT_ALLOWED = 405;
}
