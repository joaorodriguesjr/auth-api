<?php

namespace Web;

class ServiceManager
{
    private $config;

    private $cache = [];

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function getService(string $serviceName) : Service
    {
        if (array_key_exists($serviceName, $this->cache)) {
            return $this->cache[$serviceName];
        }

        $config = $this->config['services'];

        if (array_key_exists($serviceName, $config['factories'])) {
            return $this->getServiceFromFactory($serviceName, $config['factories']);
        }

        if (array_key_exists($serviceName, $config['invokables'])) {
            return $this->getServiceFromInvokable($serviceName, $config['invokables']);
        }

        throw new ServiceException("Service not found for key: '$serviceName'");
    }

    public function getServiceFromFactory(string $serviceName, array $factories) : Service
    {
        $factoryClass = $factories[$serviceName];

        if (! class_exists($factoryClass)) {
            throw new ServiceException("Factory class not found: '$factoryClass'");
        }

        $factory = new $factoryClass();

        if (! $factory instanceof ServiceFactory) {
            throw new ServiceException("Factory: '$factoryClass' must implement 'Web\ServiceFactory'");
        }

        return $this->cache($serviceName, $factory->create($this));
    }

    public function getServiceFromInvokable(string $serviceName, array $invokables) : Service
    {
        $serviceClass = $invokables[$serviceName];

        if (! class_exists($serviceClass)) {
            throw new ServiceException("Service class not found: '$serviceClass'");
        }

        return $this->cache($serviceName, new $serviceClass());
    }

    public function cache($key, $instance) : Service
    {
        if (! $instance instanceof Service) {
            throw new ServiceException("Service: '$key' must implement 'Web\Service'");
        }

        $this->cache[$key] = $instance;
        return $instance;
    }

    public function getConfig(string $key) : array
    {
        if (! array_key_exists($key, $this->config)) {
            throw new ConfigException('Configuration not found for key: ' . $key);
        }

        return $this->config[$key];
    }

    public function getCache(): array
    {
        return $this->cache;
    }
}
