<?php

namespace Web;

class RequestFactory
{
    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function createRequest() : Request
    {
        return Request::createFromGlobals();
    }
}
