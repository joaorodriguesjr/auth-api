<?php

namespace Web;

interface EventHandler extends Service
{
    public function handle(Event $event);
}
