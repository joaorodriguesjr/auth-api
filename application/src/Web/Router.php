<?php

namespace Web;

class Router implements Service
{
    private $routes;

    private $eventManager;

    public function __construct(array $routes, EventManager $eventManager)
    {
        $this->routes = $routes;
        $this->eventManager = $eventManager;
    }

    public function route(Request $request) : Route
    {
        $matchedRoute = null;
        $path = parse_url($request->getUri(), PHP_URL_PATH);

        foreach ($this->routes as $route) {
            if (! $route->match($path)) {
                continue;
            }

            $request->attributes->add($route->getParameters());
            $matchedRoute = $route;
        }

        if ($matchedRoute === null) {
            throw new RouteException('No route match for path: ' . $path, RouteException::NOT_FOUND);
        }

        if (! $matchedRoute->allows($request->getMethod())) {
            throw new RouteException('Method not allowed for path: ' . $path, RouteException::NOT_ALLOWED);
        }

        $this->eventManager->notify(new Event\RouteMatch($matchedRoute));
        return $matchedRoute;
    }
}
