<?php

namespace Web;

class Route
{
    private $name;

    private $pattern;

    private $handler;

    private $methods;

    private $parameters = ['keys'=> [], 'values' => []];

    private const MATCH_OFFSET = 1;

    public function __construct(string $pattern, $handler, array $methods, string $name)
    {
        $this->name = $name;
        $this->handler = $handler;
        $this->methods = $methods;
        $this->process($pattern);
    }

    private function process(string $pattern): void
    {
        $sections = [];

        foreach (explode('/', $pattern) as $section) {
            if (! preg_match('#{([A-Za-z0-9]+)}#', $section, $matches)) {
                array_push($sections, $section);
                continue;
            }

            array_push($sections, '([A-Za-z0-9\-_]+)');
            array_push($this->parameters['keys'], $matches[self::MATCH_OFFSET]);
        }

        $this->pattern = '#^' . implode('/', $sections) . '$#';
        return;
    }

    public function match(string $path) : bool
    {
        if (! preg_match($this->pattern, $path, $matches)) {
            return false;
        }

        $this->parameters['values'] = array_slice($matches, self::MATCH_OFFSET);
        return true;
    }

    public function getPattern(): string
    {
        return $this->pattern;
    }

    public function getHandler()
    {
        return $this->handler;
    }

    public function updateHandler($handler)
    {
        $this->handler = $handler;
    }

    public function expectsParameters() : bool
    {
        return ! empty($this->parameters['keys']);
    }

    public function allows(string $method)
    {
        return in_array($method, $this->methods);
    }

    public function withParameters($params) : self
    {
        $this->parameters['values'] = $params;
        return $this;
    }

    public function getParameters() : array
    {
        return array_combine(
            $this->parameters['keys'  ],
            $this->parameters['values']
        );
    }

    public function isNamed(string $name) : bool
    {
        return $this->name === $name;
    }
}
