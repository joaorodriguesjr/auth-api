<?php

namespace Web;

class EventManager implements Service
{
    private $handlers;

    public function __construct(array $handlers)
    {
        $this->handlers = $handlers;
    }

    public function notify(Event $event)
    {
        $key = get_class($event);

        if (! isset($this->handlers[$key])) {
            return;
        }

        foreach ($this->handlers[$key] as $handler) {
            if (! $handler instanceof EventHandler) {
                continue;
            }

            $handler->handle($event);
        }
    }
}
