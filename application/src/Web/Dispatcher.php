<?php

namespace Web;

class Dispatcher implements Service
{
    private $eventManager;

    public function __construct(EventManager $eventManager)
    {
        $this->eventManager = $eventManager;
    }

    public function dispatch(Route $route, Request $request, Response $response)
    {
        $this->eventManager->notify(new Event\PreDispatch($route, $request, $response));

        if (! $response->isSuccessful()) {
            $this->eventManager->notify(new Event\PostDispatch($route, $request, $response));
            return;
        }

        try {
            call_user_func($route->getHandler(), $request, $response);
            $this->eventManager->notify(new Event\PostDispatch($route, $request, $response));
        } catch (\Exception $exception) {
            throw new DispatchException('Application error: ', $response::HTTP_INTERNAL_SERVER_ERROR, $exception);
        }
    }
}
