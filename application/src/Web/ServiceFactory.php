<?php

namespace Web;

interface ServiceFactory
{
    public function create(ServiceManager $serviceManager) : Service;
}
