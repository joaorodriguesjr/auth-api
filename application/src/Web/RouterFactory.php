<?php

namespace Web;

class RouterFactory implements ServiceFactory
{
    public function create(ServiceManager $serviceManager): Service
    {
        $routes = [];

        foreach ($serviceManager->getConfig('routes') as $id => $route) {
            $routes[] = new Route($route['pattern'], $route['handler'], $route['methods'], $id);
        }

        return new Router($routes, $serviceManager->getService(EventManager::class));
    }
}
