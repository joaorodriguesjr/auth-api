<?php

namespace Web;

class Application implements Service
{
    private $router;

    private $dispatcher;

    private $eventManager;

    private $requestFactory;

    private $responseFactory;

    public function __construct(
        Router $router,
        Dispatcher $dispatcher,
        EventManager $eventManager,
        RequestFactory $requestFactory,
        ResponseFactory $responseFactory
    ) {
        $this->router = $router;
        $this->dispatcher = $dispatcher;
        $this->eventManager = $eventManager;
        $this->requestFactory = $requestFactory;
        $this->responseFactory = $responseFactory;
    }

    public function run() : void
    {
        $request = $this->requestFactory->createRequest();
        $response = $this->responseFactory->createResponse();

        try {
            $route = $this->router->route($request);
            $this->dispatcher->dispatch($route, $request, $response);
        } catch (RouteException $exception) {
            $this->eventManager->notify(new Event\RouteException($exception));
        } catch (DispatchException $exception) {
            $this->eventManager->notify(new Event\DispatchException($exception));
        }
    }
}
