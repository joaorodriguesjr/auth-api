<?php

namespace Web\Event;

use Web\Route;

class RouteMatch extends \Web\Event
{
    public $route;

    public function __construct(Route $route)
    {
        $this->route = $route;
    }
}
