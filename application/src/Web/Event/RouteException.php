<?php

namespace Web\Event;

class RouteException extends \Web\Event
{
    public $exception;

    public function __construct(\Web\RouteException $exception)
    {
        $this->exception = $exception;
    }
}
