<?php

namespace Web\Event\Handler;

use Web;

class DispatchExceptionResponseFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        $config = $serviceManager->getConfig('config');
        return new DispatchExceptionResponse($config['environment']);
    }
}
