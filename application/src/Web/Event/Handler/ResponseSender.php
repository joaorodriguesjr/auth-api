<?php

namespace Web\Event\Handler;

use Web;

class ResponseSender implements Web\EventHandler
{
    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\PostDispatch) {
            return;
        }

        $event->response->send();
    }
}
