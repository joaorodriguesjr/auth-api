<?php

namespace Web\Event\Handler;

use Web;

class DispatchExceptionResponse implements Web\EventHandler
{
    private $environment;

    public function __construct($environment)
    {
        $this->environment = $environment;
    }

    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\DispatchException) {
            return;
        }

        $data = $this->environment === 'development' ? $event->exception->getMessage() : null;
        $response = new Web\Response($data, 500);
        $response->send();
    }
}
