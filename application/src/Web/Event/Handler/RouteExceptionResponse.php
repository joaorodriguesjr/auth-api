<?php

namespace Web\Event\Handler;

use Web;

class RouteExceptionResponse implements Web\EventHandler
{
    private $environment;

    public function __construct($environment)
    {
        $this->environment = $environment;
    }

    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\RouteException) {
            return;
        }

        $data = $this->environment === 'development' ? $event->exception->getMessage() : null;
        $response = new Web\Response($data, $event->exception->getCode());
        $response->send();
    }
}
