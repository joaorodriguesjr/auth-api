<?php

namespace Web\Event\Handler;

use Web;

class ServiceInjector implements Web\EventHandler
{
    private $serviceManager;

    public function __construct(Web\ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function handle(Web\Event $event)
    {
        if (! $event instanceof Web\Event\RouteMatch) {
            return;
        }

        try {
            $handler = $event->route->getHandler();

            if (is_callable($handler)) {
                return;
            }

            $instance = $this->serviceManager->getService($handler);
            $event->route->updateHandler([$instance, 'handle']);
        } catch (Web\Exception $exception) {
            error_log($exception->getMessage(), LOG_NOTICE);
        }
    }
}
