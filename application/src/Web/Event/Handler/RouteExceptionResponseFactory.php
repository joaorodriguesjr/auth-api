<?php

namespace Web\Event\Handler;

use Web;

class RouteExceptionResponseFactory implements Web\ServiceFactory
{
    public function create(Web\ServiceManager $serviceManager): Web\Service
    {
        $config = $serviceManager->getConfig('config');
        return new RouteExceptionResponse($config['environment']);
    }
}
