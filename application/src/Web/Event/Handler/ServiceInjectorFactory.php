<?php

namespace Web\Event\Handler;

use Web\Service;
use Web\ServiceFactory;
use Web\ServiceManager;

class ServiceInjectorFactory implements ServiceFactory
{
    public function create(ServiceManager $serviceManager): Service
    {
        return new ServiceInjector($serviceManager);
    }
}
