<?php

namespace Web\Event;

class DispatchException extends \Web\Event
{
    public $exception;

    public function __construct(\Web\DispatchException $exception)
    {
        $this->exception = $exception;
    }
}
