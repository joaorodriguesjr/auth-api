<?php

namespace Web\Event;

use Web;

class PreDispatch extends \Web\Event
{
    public $route;

    public $request;

    public $response;

    public function __construct(Web\Route $route, Web\Request $request, Web\Response $response)
    {
        $this->route = $route;
        $this->request = $request;
        $this->response = $response;
    }
}
