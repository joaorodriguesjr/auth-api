<?php

namespace Web;

class ApplicationFactory implements ServiceFactory
{
    public function create(ServiceManager $serviceManager): Service
    {
        return new Application(
            $serviceManager->getService(Router::class),
            $serviceManager->getService(Dispatcher::class),
            $serviceManager->getService(EventManager::class),
            new RequestFactory(),
            new ResponseFactory()
        );
    }
}
