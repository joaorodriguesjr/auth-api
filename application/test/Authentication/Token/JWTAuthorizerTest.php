<?php

namespace Authentication\Token;

use PHPUnit\Framework\TestCase;
use Lcobucci\JWT;

class JWTAuthorizerTest extends TestCase
{
    public function testShouldAuthorizeAValidAndVerifiedJwtAccessToken()
    {
        $parser = $this->createMock(JWT\Parser::class);
        $signer = $this->createMock(JWT\Signer::class);
        $parsed = $this->createMock(JWT\Token::class);
        $key = new JWT\Signer\Key('jKB/cVZaDatPbtaH+TiX8w');
        $validationData = new JWT\ValidationData();

        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
            . 'eyJpYXQiOjAsImV4cCI6MzYwMCwic3ViIjoiOGQ5ZmRjNjI3ZmE4NDIwNjlkYjdkNmIzZTUzZTViNDcifQ.'
            . 'LTSxvARFvpWAw9TbrAnNwnYcprvz58-BpG2WbbRWL-I';

        $parser->method('parse')
            ->with($token)
            ->willReturn($parsed);

        $parsed->method('validate')
            ->with($validationData)
            ->willReturn(true);

        $parsed->method('verify')
            ->with($signer, $key)
            ->willReturn(true);

        $authorizer = new JWTAuthorizer($parser, $signer, $key);
        $authorizer->setValidationData($validationData);
        $result = $authorizer->authorize($token);
        $this->assertTrue($result);
    }

    public function testShouldThrowAnExceptionForANonParseableToken()
    {
        $parser = $this->createMock(JWT\Parser::class);
        $signer = $this->createMock(JWT\Signer::class);
        $key = new JWT\Signer\Key('jKB/cVZaDatPbtaH+TiX8w');

        $token = 'INVALID JWT TOKEN';
        $message = 'ERROR: Non parseable token';

        $parser->method('parse')
            ->with($token)
            ->willThrowException(new \Exception($message));

        $this->expectException(\Authentication\Token\Exception::class);
        $this->expectExceptionMessage($message);

        $authorizer = new JWTAuthorizer($parser, $signer, $key);
        $authorizer->authorize($token);
    }

    public function testShouldProvideAMethodToSetTheValidationData()
    {
        $parser = $this->createMock(JWT\Parser::class);
        $signer = $this->createMock(JWT\Signer::class);
        $key = new JWT\Signer\Key('jKB/cVZaDatPbtaH+TiX8w');
        $validationData = new JWT\ValidationData();

        $authorizer = new JWTAuthorizer($parser, $signer, $key);
        $authorizer->setValidationData($validationData);
        $this->assertEquals($validationData, $authorizer->getValidationData());
    }

    public function testShouldProvideTheValidationDataEvenIfItIsNotSet()
    {
        $parser = $this->createMock(JWT\Parser::class);
        $signer = $this->createMock(JWT\Signer::class);
        $key = new JWT\Signer\Key('jKB/cVZaDatPbtaH+TiX8w');

        $authorizer = new JWTAuthorizer($parser, $signer, $key);
        $this->assertInstanceOf(JWT\ValidationData::class, $authorizer->getValidationData());
    }
}
