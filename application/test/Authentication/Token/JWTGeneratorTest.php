<?php

namespace Authentication\Token;

use PHPUnit\Framework\TestCase;
use Lcobucci\JWT;

class JWTGeneratorTest extends TestCase
{
    public function testShouldProvideASignedJwtAccessToken()
    {
        $parser = $this->createMock(JWT\Parser::class);
        $builder = $this->createMock(JWT\Builder::class);
        $signer = $this->createMock(JWT\Signer::class);
        $privateKey = new JWT\Signer\Key('ImV4cCI6MzYwMCwic3ViIjo');
        $publicKey = new JWT\Signer\Key('NjlkYjdkNmIzZTUzZTViND');

        $time = 0;
        $plusOneHour = 300;
        $userID = '8d9fdc627fa842069db7d6b3e53e5b47';
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
            . 'eyJpYXQiOjAsImV4cCI6MzYwMCwic3ViIjoiOGQ5ZmRjNjI3ZmE4NDIwNjlkYjdkNmIzZTUzZTViNDcifQ.'
            . 'LTSxvARFvpWAw9TbrAnNwnYcprvz58-BpG2WbbRWL-I';

        $builder->method('issuedAt')
            ->with($time)
            ->willReturn($builder);

        $builder->method('expiresAt')
            ->with($plusOneHour)
            ->willReturn($builder);

        $builder->method('relatedTo')
            ->with($userID)
            ->willReturn($builder);

        $builder->method('getToken')
            ->with($signer, $privateKey)
            ->willReturn($token);

        $generator = new JWTGenerator($parser, $builder, $signer, $privateKey, $publicKey);
        $result = $generator->generate($userID, $time);
        $this->assertEquals($token, $result);
    }

    public function testShouldGenerateIssuedTimeIfNoneIsProvided()
    {
        $parser = $this->createMock(JWT\Parser::class);
        $builder = $this->createMock(JWT\Builder::class);
        $signer = $this->createMock(JWT\Signer::class);
        $privateKey = new JWT\Signer\Key('ImV4cCI6MzYwMCwic3ViIjo');
        $publicKey = new JWT\Signer\Key('NjlkYjdkNmIzZTUzZTViND');

        $userID = '8d9fdc627fa842069db7d6b3e53e5b47';
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
            . 'eyJpYXQiOjAsImV4cCI6MzYwMCwic3ViIjoiOGQ5ZmRjNjI3ZmE4NDIwNjlkYjdkNmIzZTUzZTViNDcifQ.'
            . 'LTSxvARFvpWAw9TbrAnNwnYcprvz58-BpG2WbbRWL-I';

        $builder->method('issuedAt')
            ->willReturn($builder);

        $builder->method('expiresAt')
            ->willReturn($builder);

        $builder->method('relatedTo')
            ->with($userID)
            ->willReturn($builder);

        $builder->method('getToken')
            ->with($signer, $privateKey)
            ->willReturn($token);

        $generator = new JWTGenerator($parser, $builder, $signer, $privateKey, $publicKey);
        $result = $generator->generate($userID);
        $this->assertEquals($token, $result);
    }

    public function testShouldExtractSubjectFromAToken()
    {

        $jwt = $this->createMock(JWT\Token::class);
        $parser = $this->createMock(JWT\Parser::class);
        $builder = $this->createMock(JWT\Builder::class);
        $signer = $this->createMock(JWT\Signer::class);
        $privateKey = new JWT\Signer\Key('ImV4cCI6MzYwMCwic3ViIjo');
        $publicKey = new JWT\Signer\Key('NjlkYjdkNmIzZTUzZTViND');

        $subject = '8d9fdc627fa842069db7d6b3e53e5b47';
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
            . 'eyJpYXQiOjAsImV4cCI6MzYwMCwic3ViIjoiOGQ5ZmRjNjI3ZmE4NDIwNjlkYjdkNmIzZTUzZTViNDcifQ.'
            . 'LTSxvARFvpWAw9TbrAnNwnYcprvz58-BpG2WbbRWL-I';

        $parser->method('parse')
            ->willReturn($jwt);

        $jwt->method('getClaim')
            ->with('sub')
            ->willReturn($subject);

        $generator = new JWTGenerator($parser, $builder, $signer, $privateKey, $publicKey);
        $result = $generator->extractSubject($token);
        $this->assertEquals($subject, $result);
    }

    public function testShouldThrowAnExceptionIfTheTokenParsingFails()
    {
        $parser = $this->createMock(JWT\Parser::class);
        $builder = $this->createMock(JWT\Builder::class);
        $signer = $this->createMock(JWT\Signer::class);
        $privateKey = new JWT\Signer\Key('ImV4cCI6MzYwMCwic3ViIjo');
        $publicKey = new JWT\Signer\Key('NjlkYjdkNmIzZTUzZTViND');

        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
            . 'eyJpYXQiOjAsImV4cCI6MzYwMCwic3ViIjoiOGQ5ZmRjNjI3ZmE4NDIwNjlkYjdkNmIzZTUzZTViNDcifQ.'
            . 'LTSxvARFvpWAw9TbrAnNwnYcprvz58-BpG2WbbRWL-I';

        $parser->method('parse')
            ->willThrowException(new \Exception('Invalid Token'));


        $generator = new JWTGenerator($parser, $builder, $signer, $privateKey, $publicKey);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid Token');
        $generator->parse($token);
    }

    public function testShouldRegenerateASignedToken()
    {

        $jwt = $this->createMock(JWT\Token::class);
        $parser = $this->createMock(JWT\Parser::class);
        $builder = $this->createMock(JWT\Builder::class);
        $signer = $this->createMock(JWT\Signer::class);
        $privateKey = new JWT\Signer\Key('ImV4cCI6MzYwMCwic3ViIjo');
        $publicKey = new JWT\Signer\Key('NjlkYjdkNmIzZTUzZTViND');

        $subject = '8d9fdc627fa842069db7d6b3e53e5b47';
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
            . 'eyJpYXQiOjAsImV4cCI6MzYwMCwic3ViIjoiOGQ5ZmRjNjI3ZmE4NDIwNjlkYjdkNmIzZTUzZTViNDcifQ.'
            . 'LTSxvARFvpWAw9TbrAnNwnYcprvz58-BpG2WbbRWL-I';

        $parser->method('parse')
            ->willReturn($jwt);

        $jwt->method('verify')
            ->with($signer, $publicKey)
            ->willReturn(true);

        $jwt->method('getClaim')
            ->with('sub')
            ->willReturn($subject);

        $builder->method('issuedAt')
            ->willReturn($builder);

        $builder->method('expiresAt')
            ->willReturn($builder);

        $builder->method('relatedTo')
            ->with($subject)
            ->willReturn($builder);

        $builder->method('getToken')
            ->with($signer, $privateKey)
            ->willReturn($token);

        $generator = new JWTGenerator($parser, $builder, $signer, $privateKey, $publicKey);
        $result = $generator->regenerate($token);
        $this->assertEquals($token, $result);
    }

    public function testShouldThrowAnExceptionIfTokenSignatureIsInvalid()
    {
        $jwt = $this->createMock(JWT\Token::class);
        $parser = $this->createMock(JWT\Parser::class);
        $builder = $this->createMock(JWT\Builder::class);
        $signer = $this->createMock(JWT\Signer::class);
        $privateKey = new JWT\Signer\Key('ImV4cCI6MzYwMCwic3ViIjo');
        $publicKey = new JWT\Signer\Key('NjlkYjdkNmIzZTUzZTViND');

        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.'
            . 'eyJpYXQiOjAsImV4cCI6MzYwMCwic3ViIjoiOGQ5ZmRjNjI3ZmE4NDIwNjlkYjdkNmIzZTUzZTViNDcifQ.'
            . 'LTSxvARFvpWAw9TbrAnNwnYcprvz58-BpG2WbbRWL-I';

        $jwt->method('verify')
            ->with($signer, $publicKey)
            ->willReturn(false);

        $parser->method('parse')
            ->willReturn($jwt);

        $generator = new JWTGenerator($parser, $builder, $signer, $privateKey, $publicKey);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Invalid token signature');
        $generator->regenerate($token);
    }
}
