<?php

require __DIR__ . '/../vendor/autoload.php';

(function () {
    $configuration = require __DIR__ . '/../config/application.php';

    $serviceManager = new Web\ServiceManager($configuration);
    $application = $serviceManager->getService(Web\Application::class);

    $application->run();
})();
