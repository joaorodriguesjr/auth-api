<?php

return [
    'services' => [
        'factories' => [
            // ******************************* Framework Main Components *********************************************//

            'Web\Router'       => 'Web\RouterFactory',
            'Web\Dispatcher'   => 'Web\DispatcherFactory',
            'Web\Application'  => 'Web\ApplicationFactory',
            'Web\EventManager' => 'Web\EventManagerFactory',

            // ******************************* Framework Event Handlers **********************************************//

            'Web\Event\Handler\RouteExceptionResponse'    => 'Web\Event\Handler\RouteExceptionResponseFactory',
            'Web\Event\Handler\DispatchExceptionResponse' => 'Web\Event\Handler\DispatchExceptionResponseFactory',
            'Web\Event\Handler\ServiceInjector'           => 'Web\Event\Handler\ServiceInjectorFactory',

            // ******************************* Application Services **************************************************//

            'Api\Services\DataBaseService' => 'Api\Services\DataBaseServiceFactory',
            'Api\Services\JwtService'      => 'Api\Services\JwtServiceFactory',

            // ******************************* Application Event Handlers ********************************************//

            'Api\EventHandlers\CorsPolicy'         => 'Api\EventHandlers\CorsPolicyFactory',
            'Api\EventHandlers\CredentialsChecker' => 'Api\EventHandlers\CredentialsCheckerFactory',
            'Api\EventHandlers\CookieChecker'      => 'Api\EventHandlers\CookieCheckerFactory',
            'Api\EventHandlers\CookieSender'       => 'Api\EventHandlers\CookieSenderFactory',
            'Api\EventHandlers\RequestDecoder'     => 'Api\EventHandlers\RequestDecoderFactory',
            'Api\EventHandlers\ResponseEncoder'    => 'Api\EventHandlers\ResponseEncoderFactory',

            // ******************************* Application Request Handlers ******************************************//

            'Api\RequestHandlers\RootHandler' => 'Api\RequestHandlers\RootHandlerFactory',
        ],
        'invokables' => [
            // ******************************* Framework Event Handlers **********************************************//

            'Web\Event\Handler\ResponseSender' => 'Web\Event\Handler\ResponseSender',
        ],
    ],
    'events' => [
        'Web\Event\RouteMatch' => [
            'Web\Event\Handler\ServiceInjector',
        ],
        'Web\Event\PreDispatch' => [
            'Api\EventHandlers\RequestDecoder',
            'Api\EventHandlers\CorsPolicy',
            'Api\EventHandlers\CookieChecker',
            'Api\EventHandlers\CredentialsChecker',
        ],
        'Web\Event\PostDispatch' => [
            'Api\EventHandlers\CookieSender',
            'Api\EventHandlers\ResponseEncoder',
            'Web\Event\Handler\ResponseSender',
        ],
        'Web\Event\RouteException' => [
            'Web\Event\Handler\RouteExceptionResponse',
        ],
        'Web\Event\DispatchException' => [
            'Web\Event\Handler\DispatchExceptionResponse',
        ],
    ],
    'routes' => [
        'authentication' => [
            'pattern' => '/',
            'handler' => 'Api\RequestHandlers\RootHandler',
            'methods' => ['OPTIONS', 'GET', 'POST', 'PUT', 'DELETE'],
        ],
    ],
    'config' => [
        'environment' => getenv('APPLICATION_ENV'),
    ],
];
