-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema authapi
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `authapi` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `authapi` ;

-- -----------------------------------------------------
-- Table `authapi`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `authapi`.`users` (
  `id` BINARY(16) NOT NULL,
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `authapi`.`credentials`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `authapi`.`credentials` (
  `identity` VARCHAR(200) NOT NULL,
  `password` VARCHAR(200) NOT NULL,
  `owner` BINARY(16) NOT NULL,
  UNIQUE INDEX `username_UNIQUE` (`identity` ASC),
  PRIMARY KEY (`owner`),
  CONSTRAINT `fk_credentials_users`
    FOREIGN KEY (`owner`)
    REFERENCES `authapi`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `authapi`.`authentications`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `authapi`.`authentications` (
  `id` BINARY(16) NOT NULL,
  `token` TEXT NOT NULL,
  `generated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `invalidated` DATETIME NULL DEFAULT NULL,
  `expires` DATETIME NOT NULL,
  `owner` BINARY(16) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_authentications_users_idx` (`owner` ASC),
  CONSTRAINT `fk_authentications_users`
    FOREIGN KEY (`owner`)
    REFERENCES `authapi`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
